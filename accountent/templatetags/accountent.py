from django.db.models import Sum
from django import template

register = template.Library()

def cut(value, arg):
    """Removes all values of arg from the given string"""
    return value.replace(arg, '')

def total_amount(value):
    return value.legs.debits().aggregate(Sum("amount"))["amount__sum"]

def get_currency(value):
    return value.legs.first().amount.currency

def as_rate(value):
    return value * 100

register.filter('get_currency', get_currency)
register.filter('total_amount', total_amount)
register.filter('as_rate', as_rate)