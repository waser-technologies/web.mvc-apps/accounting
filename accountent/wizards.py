import os.path
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from formtools.wizard.views import SessionWizardView
from django.db import transaction as db_transaction
from django.conf import settings
from django.urls import reverse_lazy
from django.core.files.storage import FileSystemStorage
from vat.forms import CompanyForm
from vat.utils import register_company

from hordak import models as hordak_models
from hordak import views as hordak_views
from moneyed import Money

from . import forms
from . import models

class RegisterWizard(SessionWizardView):
    template_name = "accountent/wizard/register_wizard.html"
    form_list = [
        CompanyForm, #0
        forms.accountentSettingsForm, #1
    ]

    def done(self, form_list, **kwargs):
        #do_something_with_the_form_data(form_list)
        form_data = [form.cleaned_data for form in form_list]

        company_form_data = form_data[0]
        
        company_form = CompanyForm(company_form_data)
        if company_form.is_valid():
            company = company_form.save()

        #create default settings
        accountent_settings_form = form_data[1]
        accountent_settings = forms.accountentSettingsForm(accountent_settings_form)
        if accountent_settings.is_valid():
            accountent_settings_model = accountent_settings.save(commit=False)
            accountent_settings_model.company = company
            accountent_settings_model.save()
        register_company(company_form_data)
        return redirect(reverse_lazy('accountent:bank_account_create'))


class TransactionReconcileWizard(SessionWizardView):
    template_name = "accountent/wizard/transaction_reconcile_wizard.html"
    form_list = [
        forms.SelectReconcileTransactionModelForm, #0
        forms.CreateReconcileTransactionModelForm, #1
        forms.VATReconcileTransactionModelForm #2
        
    ]

    def get_statement_line(self):
        sl = hordak_models.StatementLine.objects.order_by('date').all()
        unreconciled_sl = [x for x in sl if not x.is_reconciled]
        return unreconciled_sl[0]

    def get_context_data(self, form, **kwargs):
        context = super(TransactionReconcileWizard, self).get_context_data(form=form, **kwargs)

        statementline = self.get_statement_line()

        global_context_data = {
            'statement': statementline
        }

        context.update(global_context_data)
        return context
    
    def get_form(self, step=None, data=None, files=None):

        form = super(TransactionReconcileWizard, self).get_form(step, data, files)
        statementline = self.get_statement_line()
        if step == '0':
            transaction_date = statementline.date
            form.fields['transaction'].queryset = models.TransactionModel.objects.filter(hordak__date=transaction_date, hordak__balance=statementline.amount)
        elif step == '1':
            code_type_data = self.get_cleaned_data_for_step('0')
            code_type = code_type_data['code_type']
            form.fields['code'].queryset = models.VATCodeModel.objects.filter(code_type=code_type)
        elif step == '2':
            vat_code_data = self.get_cleaned_data_for_step('1')
            vat_debit_account = vat_code_data['code'].debit_account
            vat_debit_account_children = vat_debit_account.get_children()
            form.fields['from_account'].queryset = vat_debit_account_children

        return form

    def done(self, form_list, **kwargs):
        form_data = [form.cleaned_data for form in form_list]
        statementline = self.get_statement_line()
        code_type=form_data[0].get('code_type')

        vat_code_data = form_data[1]
        vat_code = vat_code_data.get('code')
        vat_rate = vat_code.rate

        amount_ht = form_data[2].get('amount') #1%-vat_rate%
        amount_vat = amount_ht * vat_rate #vat_rate%
        amount_total = amount_ht + amount_vat #1%

        account_debit_1 = form_data[2]['from_account']
        account_debit_2 = vat_code.credit_account
        account_credit = form_data[2]['to_account']

        date = form_data[2]['date']
        description = form_data[2]['description']

        with db_transaction.atomic():
            hordak_transaction = hordak_models.Transaction.objects.create(date=date, description=description)
            hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_debit_1, amount=amount_ht)
            hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_debit_2, amount=amount_vat)
            hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_credit, amount=-1*amount_total)

            transaction = models.TransactionModel.objects.create(hordak=hordak_transaction, code=vat_code, code_type=code_type)
        return redirect('accountent:transactions_list')

class TransactionCreateWizard(SessionWizardView):
    template_name = "accountent/wizard/transactions_create_wizard.html"
    form_list = [
        forms.TransactionTypeModelForm, #0
        forms.TransactionVATCodeModelForm, #1
        forms.BaseTransactionAccountForm, #2
    ]
    
    def get_form(self, step=None, data=None, files=None):

        form = super(TransactionCreateWizard, self).get_form(step, data, files)

        if step == '1':
            code_type_data = self.get_cleaned_data_for_step('0')
            code_type = code_type_data['code_type']
            form.fields['code'].queryset = models.VATCodeModel.objects.filter(code_type=code_type)
        elif step == '2':
            vat_code_data = self.get_cleaned_data_for_step('1')
            if vat_code_data.get('code') == "EI":
                vat_credit_account = vat_code_data['code'].credit_account
                vat_credit_account_children = vat_credit_account.get_children()
                form.fields['to_account'].queryset = vat_credit_account_children
            else:
                vat_debit_account = vat_code_data['code'].debit_account
                vat_debit_account_children = vat_debit_account.get_children()
                form.fields['from_account'].queryset = vat_debit_account_children

        return form

    def done(self, form_list, **kwargs):
        form_data = [form.cleaned_data for form in form_list]

        code_type=form_data[0].get('code_type')

        date = form_data[2]['date']
        description = form_data[2]['description']

        vat_code_data = form_data[1]
        vat_code = vat_code_data.get('code')

        account_debit_1 = form_data[2]['from_account']
        account_credit = form_data[2]['to_account']

        if code_type == "SV":
            vat_rate = vat_code.rate

            amount_ht = form_data[2].get('amount') #1%-vat_rate%
            amount_vat = amount_ht * vat_rate #vat_rate%
            amount_total = amount_ht + amount_vat #1%

            account_debit_1 = form_data[2]['from_account']
            account_debit_2 = vat_code.credit_account
            account_credit = form_data[2]['to_account']
        
            with db_transaction.atomic():
                hordak_transaction = hordak_models.Transaction.objects.create(date=date, description=description)
                hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_debit_1, amount=amount_ht)
                hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_debit_2, amount=amount_vat)
                hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_credit, amount=-1*amount_total)

                transaction = models.TransactionModel.objects.create(hordak=hordak_transaction, code=vat_code, code_type=code_type)
        elif code_type == "S":

            amount = form_data[2].get('amount')

            with db_transaction.atomic():
                hordak_transaction = hordak_models.Transaction.objects.create(date=date, description=description)
                hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_debit_1, amount=amount)
                hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_credit, amount=-1*amount)

                transaction = models.TransactionModel.objects.create(hordak=hordak_transaction, code=vat_code, code_type=code_type)
        
        elif code_type == "EI":
            vat_rate = vat_code.rate

            amount_ht = form_data[2].get('amount') #1%-vat_rate%
            amount_vat = amount_ht * vat_rate #vat_rate%
            amount_total = amount_ht + amount_vat #1%

            account_debit_1 = form_data[2]['from_account']
            account_debit_2 = vat_code.credit_account
            account_credit = form_data[2]['to_account']
        
            with db_transaction.atomic():
                hordak_transaction = hordak_models.Transaction.objects.create(date=date, description=description)
                hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_debit_1, amount=amount_ht)
                hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_debit_2, amount=amount_vat)
                hordak_models.Leg.objects.create(transaction=hordak_transaction, account=account_credit, amount=-1*amount_total)

                transaction = models.TransactionModel.objects.create(hordak=hordak_transaction, code=vat_code, code_type=code_type)

        return redirect('accountent:transactions_list')
