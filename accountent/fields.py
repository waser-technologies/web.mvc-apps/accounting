from django.db.models import fields
from django import forms

class PercentageField(fields.FloatField):
    widget = forms.TextInput(attrs={"class": "percentInput"})

    def is_number(self, value):
        try:
            val = float(value)
            return True
        except expression as identifier:
            return False

    def to_python(self, value):
        val = super(PercentageField, self).to_python(value)
        if self.is_number(val):
            return val/100
        return val

    def prepare_value(self, value):
        val = super(PercentageField, self).prepare_value(value)
        if self.is_number(val) and not isinstance(val, str):
            return str((float(val)*100))
        return val