import os
import json, yaml
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from hordak import models as hordak_models
from . import models


def get_or_create_account(name, code, account_type, parent_uuid=None, currency=settings.DEFAULT_CURRENCY, is_bank_account=False):
    if os.environ.get('POSTGRES_HOST', False):
        from hordak.models import Account
        if parent_uuid:
            parent = hordak_models.Account.objects.get(uuid=parent_uuid)
        else:
            parent = parent_uuid
        try:
            account = Account.objects.get(code=code, parent=parent)
        except Exception as e:
            account, _ = Account.objects.get_or_create(name=name, code=code, parent=parent, type=account_type, currencies=[currency], is_bank_account=is_bank_account)
        
        return account


def get_bank_accounts():
    if os.environ.get('POSTGRES_HOST', False):
        from hordak.models import Account
        
        parent = Account.objects.filter(full_code=102).first()
        return Account.objects.filter(parent=parent, is_bank_account=True).all()

    return None

def create_bank_account(name, currency=None):
    if os.environ.get('POSTGRES_HOST', False):
        from hordak.models import Account

        if not currency:
            currencies = [settings.DEFAULT_CURRENCY]
        else:
            currencies = [currency]

        parent = Account.objects.filter(full_code=102).first()
        children_length = len(parent.children.all())
        new_code = children_length
        bank_account = Account.objects.create(name=name, parent=parent, code=new_code, type=parent.type, is_bank_account=True, currencies=currencies)

        return bank_account

def get_account_type(f_code_str):
    if os.environ.get('POSTGRES_HOST', False):
        from hordak.models import Account
        TYPE_CHOICES = Account.TYPES
        code = int(f_code_str[0])
        code_matrix = [
            TYPE_CHOICES.asset,
            TYPE_CHOICES.liability,
            TYPE_CHOICES.income,
            TYPE_CHOICES.expense,
            TYPE_CHOICES.equity,
            TYPE_CHOICES.trading,
        ]
        c = 0
        if code == 1:
            c = 0
        elif code == 2:
            c = 1
        elif code == 3:
            c = 2
        elif code >= 4 and code <= 6:
            c = 3
        elif code == 7:
            c = 2
        elif code == 8:
            if len(f_code_str) == 1:
                c = 3
            elif len(f_code_str) == 2: 
                if int(f_code_str[1]) == 1:
                    c = 2
                else:
                    c = 3
            elif len(f_code_str) >= 3:
                if int(f_code_str[1]) == 1:
                    c = 2
                elif (int(f_code_str[1]) == 5 and int(f_code_str[2]) == 1):
                    c = 2
                else:
                    c = 3
            else:
                c = 3
        elif code == 9:
            c = 4
        else:
            c = 5
        
        account_type = code_matrix[c]
        return account_type

def is_account_bank(f_code_int):
    if (f_code_int >= 1020) and (f_code_int <= 1029):
        return True
    else:
        return False

def import_accounts_from_txt(filename):

    accounts_txt_path = os.path.join(settings.STATIC_ROOT, "accountent", filename + ".txt")
    accounts_file = open(accounts_txt_path, 'r')
    accounts_txt = accounts_file.read()
    accounts_file.close()

    accounts_dict = {}

    for line in accounts_txt.split("\n"):
        f_code = []
        separator = ";"
        account_csv = line.split(separator)
        for c in account_csv[0]:
            f_code.append(int(c))
        f_code_str = str(account_csv[0])
        account_name = str(account_csv[1][:49])
        accounts_dict[f_code_str] = {
            'f_code': f_code,
            'name': account_name
        }
        if len(f_code) == 1:
            parent_code = None
            parent_uuid = None
        elif len(f_code) == 2:
            parent_code = str(f_code[0])
        elif len(f_code) == 3:
            parent_code = "".join(str(c) for c in f_code[0:2])
        elif len(f_code) == 4:
            parent_code = "".join(str(c) for c in f_code[0:3])
        
        if parent_code:
            parent_uuid = accounts_dict[parent_code].get('uuid')
        
        currency = settings.DEFAULT_CURRENCY
        account_type = get_account_type(str(account_csv[0]))
        is_bank_account = is_account_bank(int(account_csv[0]))
        account = get_or_create_account(account_name, f_code[-1], parent_uuid=parent_uuid, account_type=account_type, currency=currency, is_bank_account=is_bank_account)
        accounts_dict[f_code_str]['uuid'] = account.uuid
        accounts_dict[f_code_str]['code'] = f_code[-1]
        accounts_dict[f_code_str]['parent_code'] = parent_code
        accounts_dict[f_code_str]['parent_uuid'] = parent_uuid
        accounts_dict[f_code_str]['account_type'] = account_type
        accounts_dict[f_code_str]['is_bank_account'] = is_bank_account

    return accounts_dict


def import_vat_codes_from_txt(filename="vat_codes"):
    vat_codes_txt_path = os.path.join(settings.STATIC_ROOT, "accountent", filename + ".txt")
    vat_codes_file = open(vat_codes_txt_path, 'r')
    vat_codes_txt = vat_codes_file.read()
    vat_codes_file.close()

    vat_codes_list = vat_codes_txt.split("\n")

    if os.environ.get('POSTGRES_HOST', False):
        from hordak.models import Account

        for vat_code in vat_codes_list:
            code, rate, deductible_at, d_account_f_code, c_account_f_code, comment, code_type = vat_code.split(";")
            d_account = Account.objects.get(full_code=int(d_account_f_code))
            c_account = Account.objects.get(full_code=int(c_account_f_code))
            models.VATCodeModel.objects.create(code=code, rate=float(rate), deductible_at=float(deductible_at), debit_account=d_account, credit_account=c_account, comment=comment, code_type=code_type)

    return vat_codes_list

def create_accounts_from_txt(filename="inde.txt"):
    #import accounts
    accounts_dict = import_accounts_from_txt(filename)
    #f = open("accountent/accounts.yml", 'w', encoding="utf8")
    #yaml.dump(accounts_dict, f, Dumper=yaml.CDumper)
    #f.close()

    #import tva code
    vat_code_list = import_vat_codes_from_txt()
    #f = open("accountent/vat_codes.yml", 'w', encoding="utf8")
    #yaml.dump(vat_code_list, f, Dumper=yaml.CDumper)
    #f.close()
    return True
