from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from hordak.forms.accounts import AccountForm
from hordak.forms import statement_csv_import as hordak_statement_forms
from hordak import forms as hordak_forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset
from hordak import models as hordak_models
#from django_iban.fields import IBANField, SWIFTBICField
from localflavor.generic.forms import BICFormField, IBANFormField

from . import models


class accountentSettingsForm(forms.ModelForm):

    class Meta:
        model = models.accountentSettings
        exclude = (
            'company', 
        )

class TransactionTypeModelForm(forms.ModelForm):
    
    class Meta:
        model = models.TransactionModel
        fields = (
            'code_type',
        )

class TransactionVATCodeModelForm(forms.ModelForm):
    
    class Meta:
        model = models.TransactionModel
        fields = (
            'code',
        )

class BaseTransactionAccountForm(hordak_forms.SimpleTransactionForm):
    pass

class CreateBankAccountForm(forms.Form):
    name = forms.CharField(max_length=50, label=_("Account name"), help_text=_("This name will be used for accountent purposes"))
    iban_number = IBANFormField(label=_('Account number (IBAN)'), help_text=_("e.g {country_code}[0-9]*25"))
    swift_bic = BICFormField(label=_('Bank SWIFT / BIC'))
    bank_name = forms.CharField(max_length=255, label=_("Bank name"), help_text=_("This name will be used for invoices purposes"))

#BankTransactionCsvImportFormSet = forms.inlineformset_factory(form=ImportBankAccountForm, extra=1, can_delete=True)


class BankTransactionCsvImportFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(BankTransactionCsvImportFormHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.layout = Layout(
            Fieldset(_("Bank statement csv file"), 'file'),
            Fieldset(_("Has headings"), 'has_headings'),
        )


class BankTransactionCsvImportForm(hordak_statement_forms.TransactionCsvImportForm):

    @property
    def helper(self):
        helper = BankTransactionCsvImportFormHelper()
        helper.form_tag = False

        return helper
    
    def save(self, commit=True):
        form_data = super(BankTransactionCsvImportForm, self).save(commit=False)

        accounts = utils.get_bank_accounts()

        form_data['accounts'] = accounts

        return form_data.save(commit=commit)
        
    def is_valid(self):
        return super().is_valid()

#BankTransactionCsvImportFormSet = forms.models.inlineformset_factory(parent_model=hordak_models.Account, model=hordak_statement_forms.TransactionCsvImport, form=BankTransactionCsvImportForm, fields=('file', 'has_headings'), extra=1, can_delete=True)

BankTransactionCsvImportColumnFormSet = hordak_statement_forms.TransactionCsvImportColumnFormSet()

class SelectReconcileTransactionModelForm(forms.Form):
    #transaction = forms.MultipleChoiceField(choices=transaction_choices, label=_("Transactions"), widget=forms.CheckboxSelectMultiple())
    pass

class CreateReconcileTransactionModelForm():
    pass

class VATReconcileTransactionModelForm():
    pass