import os
from datetime import date

from django.db import models
from django.utils.translation import ugettext_lazy as _
from hordak.models import Account, Transaction
from vat.models import CompanyModel, STATEMENT_REPORT_TYPE, STATEMENT_REPORT_FORM
from localflavor.generic.models import BICField, IBANField
from django_countries.fields import CountryField

#from django.db.models.signals import post_save

from . import fields

TVA_CODE_CHOICES = [
    ('TVA', _("VAT")),
    ('TVARED', _("VAT Reduced")),
    ('TVAHEB', _("Hosting VAT")),
    ('APSETR', _("Forein services")),
    ('DIMCP', _("Norm. rate diminution")),
    ('DIMCPRED', _("Reduced rate diminution")),
    ('EXONERE', _("Exonerated services")),
    ('EXPORT', _("Exportation")),
    ('EXCLU', _("Not imposable")),
    ('PSETR', _("Foreign services")),
    ('IPI', _("Input tax normal rate")),
    ('IPIRED', _("Input tax reduced rate")),
    ('IPM', _("Input tax on stock normal rate")),
    ('IPMRED', _("Input tax on stock reduced rate")),
    ('DEGR', _("Subsequent tax relief")),
    ('COR', _("Correction of input tax, normal rate")),
    ('CORRED', _("Correction of input tax, reduced rate")),
    ('MF1', _("Subsidies, tourist taxes and the like. Contributions paid to establishments responsible for waste disposal and water supply")),
    ('MF2', _("Donations, dividends, compensation, etc.")),
]

TVA_CODE_CHOICES_DEFAULT = TVA_CODE_CHOICES[0][0]

TVA_CODE_TYPE_CHOICES = [
    ('SV', _("Sell with VAT")),
    ('S', _("Sell without VAT")),
    ('EI', _("Expenses and Investisments")),
    ('MF', _("Other movement of funds")),
]

TVA_CODE_TYPE_CHOICES_DEFAULT = TVA_CODE_TYPE_CHOICES[0][0]

class Bank(models.Model):
    name = models.CharField(max_length=255)
    # describe establishment
    street = models.CharField(_('Bank street and number'), max_length=255, blank=True)
    zip_code = models.CharField(_('Bank ZIP'), max_length=255, blank=True)
    city = models.CharField(_('Bank city'), max_length=255, blank=True)
    country = CountryField(_('Bank country'), max_length=255, blank=True)

class BankAccount(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)  
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    iban = IBANField(verbose_name=_('Account number (IBAN)'))
    swift_bic = BICField(verbose_name=_('Bank SWIFT / BIC'))


class VATCodeModel(models.Model):
    code = models.CharField(max_length=16, choices=TVA_CODE_CHOICES, default=TVA_CODE_CHOICES_DEFAULT)
    rate = fields.PercentageField()
    deductible_at = fields.PercentageField(default=1)
    debit_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="debit_vat_account")
    credit_account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="credit_vat_account")
    comment = models.TextField()
    code_type = models.CharField(max_length=4, choices=TVA_CODE_TYPE_CHOICES, default=TVA_CODE_TYPE_CHOICES_DEFAULT)

    def __str__(self):
        return str(self.code) + str(self.debit_account.full_code)

class TransactionModel(models.Model):
    hordak = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    code = models.ForeignKey(VATCodeModel, on_delete=models.CASCADE)
    code_type = models.CharField(max_length=4, choices=TVA_CODE_TYPE_CHOICES, default=TVA_CODE_TYPE_CHOICES_DEFAULT)

class AccountCode(models.Model):
    account = models.OneToOneField(Account, on_delete=models.CASCADE)
    vat_code = models.ForeignKey(VATCodeModel, on_delete=models.CASCADE)

class accountentSettings(models.Model):
    company = models.OneToOneField(CompanyModel, on_delete=models.CASCADE, primary_key=True)
    vat_inscription = models.DateField(blank=True, null=True)
    default_report_period = models.PositiveSmallIntegerField(default=3, verbose_name=_("Default report period"), help_text=_("Time in month"))
    default_submission_type = models.IntegerField(choices=STATEMENT_REPORT_TYPE, default=1)
    default_report_form = models.IntegerField(choices=STATEMENT_REPORT_FORM, default=1)

    class Meta:
        verbose_name = _("accountent Settings")
        verbose_name_plural = _("accountent Settings")

    def __str__(self):
        return "[settings]:" + self.company.name

""" def post_save_settings_create(sender, instance, created, *args, **kwargs):
    if created:
        accountentSettings.objects.get_or_create(company=instance)

    company, created = accountentSettings.objects.get_or_create(company=instance)
    company.save()


post_save.connect(post_save_settings_create, sender=CompanyModel) """
