1;Actifs
10;Actifs circulants
100;Liquidités
1000;Caisse
102;Avoirs bancaires
1020;Banque
106;Avoirs à court terme cotés en bourse
1060;Titres
1069;Ajustement de la valeur des titres
11;Créances à court terme
110;Créances résultant de livraisons et prestations
1100;Créances provenant de livraisons et de prestations (Débiteurs)
1109;Ducroire
111;Créances résultant de livr. et prest
1110;Créances résultant de livr. et prest. envers les sociétés du groupe
114;Autres créances à court terme
1140;Avances et prêts
1149;Ajustement de la valeur des avances et des prêts
117;Impôt
1170;Impôt préalable: TVA s/matériel, marchandises, prestations et énergie
1171;Impôt préalable: TVA s/investissements et autres charges d’exploitation
1176;Impôt anticipé
118;Assurances
1180;Créances envers les assurances sociales et institutions de prévoyance
1189;Impôt à la source
119;ACE
1190;Autres créances à court terme
1199;Ajustement de la valeur des créances à court terme
12;Stocks et prestations
120;Stocks et prestations non facturées
1200;Marchandises commerciales
121;Matières premières
1210;Matières premières
122;Matières auxiliaires
1220;Matières auxiliaires
123;Matières consommables
1230;Matières consommables
125;Marchandises en consignation
1250;Marchandises en consignation
126;Stocks de produits
1260;Stocks de produits finis
128;Travaux
1280;Travaux en cours
13;Comptes de régularisation
130;Compte de régularisation de l’actif
1300;Charges payées d‘avance
1301;Produits à recevoir
14;Actifs immobilisés
140;Immobilisations financières
1400;Titres à long terme
1409;Ajustement de la valeur des titres
144;Prêts
1440;Prêts
1441;Hypothèques
1449;Ajustement de la valeur des créances à long terme
148;Participations
1480;Participations
1489;Ajustement de la valeur des participations
15;Ajustements
150;Immobilisations corporelles meubles
1500;Machines et appareils
1509;Ajustement de la valeur des machines et appareils
151;Mobilier et installations
1510;Mobilier et installations
1519;Ajustement de la valeur du mobilier et des installations
152;Machines de bureau, informatique, systèmes de communication
1520;Machines de bureau, informatique, systèmes de communication
1529;Ajustement de la valeur des machines de bureau, inf. et syst. comm.
153;Véhicules
1530;Véhicules
1539;Ajustement de la valeur des véhicules
154;Outillages et appareils
1540;Outillages et appareils
1549;Ajustement de la valeur des outillages et appareils
16;Immeubles
160;Immobilisations corporelles immeubles
1600;Immeubles d’exploitation
1609;Ajustement de la valeur des immeubles d’exploitation
17;Immobilisations incorporelles
170;Immobilisations incorporelles
1700;Brevets, know-how, licences, droits, développement
1709;Ajustement de la valeur des brevets, know-how, licences, droits, dév.
177;Goodwill
1770;Goodwill
1779;Ajustement de la valeur des goodwill
18;Capital
180;Capital non versé : capital social, capital de fondation
185;Capital actions, capital social, droits de participations ou capital de fondation
1850;Capital actions, capital social, droits de participations ou capital de fondation non versés
2;Passif
20;Dettes à court terme
200;Dettes à court terme résultant d’achats et de prestations de services
2000;Dettes résultant d’achats et de prestation de services (créanciers)
203;Acomptes
2030;Acomptes de clients
205;Prestations de services envers des sociétés du groupe
2050;Dettes résultant d’achats et de prestations de services envers des sociétés du groupe
21;Dettes à court terme rémunéréss
210;Dettes à court terme rémunérés
2100;Dettes bancaires
212;Leasing
2120;Engagements de financement par leasing
214;Dette rémunérées
2140;Autres dettes à court terme rémunérées
22;Autres dettes à court terme
220;Préstation exonérées
2200;TVA due
2201;Décompte TVA
2206;Impôt anticipé dû
2208;Impôts directs
221;Autres dettes à court terme
2210;Autres dettes à court terme
226;Actionnaires
2261;Dividendes
227;Assurances
2270;Assurances sociales et institutions de prévoyance
2279;Impôt à la source
23;Impôts
230;Passifs de régularisation et provisions à court terme
2300;Charges à payer
2301;Produits encaissés d’avance
233;Provisions
2330;Provisions à court terme
235;Diminution de la contre-prestation
24;Dettes à long terme
240;Dettes à long terme rémunérées
2400;Dettes bancaires
242;Leasing
2420;Engagements de financement par leasing
243;Emprunts obligataires
2430;Emprunts obligataires
245;Emprunts
2450;Emprunts
2451;Hypothèques
25;Autres dettes à long terme
250;Autres dettes à long terme
2500;Autres dettes à long terme
26;Provisions
260;Provisions à long termes et provisions légales
2600;Provisions
28;Fonds propres (personnes morales)
280;Capital social ou capital de fondation
2800;Capital-actions, capital social, capital de fondation
29;Réserves / bénéfices et pertes
290;Réserves / bénéfices et pertes
2900;Réserves légales issues du capital
293;Réserves sur participations propres
2930;Réserves sur participations propres au capital
294;Réserves d‘évaluation
2940;Réserves d‘évaluation
295;Réserves légales issues du bénéfice
2950;Réserves légales issues du bénéfice
296;Réserves libres
2960;Réserves libres
297;Bénéfice / perte reporté
2970;Bénéfice / perte reporté
2979;Bénéfice / perte de l’exercice
298;Propres actions, parts sociales, droits de participations
2980;Propres actions, parts sociales, droits de participations (poste négatif)
3;Chiffre d’affaires résultant des ventes et des prestations de services
30;Ventes taux normal
300;Ventes de produits
3000;Ventes de produits fabriqués
31;Ventes taux réduit
310;Ventes de produits
3100;Ventes de produits fabriqués
32;Ventes de marchandises
320;Ventes de marchandises
3200;Ventes de marchandises
34;Ventes de prestations
340;Ventes de prestations (hébérgement)
3400;Ventes de prestations
36;Autres ventes et prestations
360;Autres ventes et prestations de services
3600;Autres ventes et prestations de services
37;Prestations propres
370;Prestations propres
3700;Prestations propres
371;Consommations propres
3710;Consommations propres
38;Déductions et pertes
380;Prestations étrangères acquisess
3800;Déductions sur ventes
3805;Pertes sur clients, variation du ducroire
39;Variation
390;Variation des stocks
3900;Variation des stocks de produits semi-finis
3901;Variation des stocks de produits finis
394;Variation des prestations
3940;Variation de la valeur des prestations non facturées
4;Charges de matériel, de marchandises et de prestations de tiers
40;Charges de matériel
400;Charges de matériel
4000;Charges de matériel de l‘atelier
405;Impôt préalable sur l'achat
4051;Investissements et d’autres charges d’exploitation au taux normal
4052;Investissements et d’autres charges d’exploitation au taux réduit
4053;Marchandises au taux normal
4054;Marchandises au taux réduit
42;Achats de marchandises destinées à la revente
420;Achats de marchandises destinées à la revente
4200;Achats de marchandises destinées à la revente
43;Prestations non-imposables
430;Prestations non-imposables
4300;Opération exclues du champs de l'impôt
4301;Prestations fournies à l'étranger
44;Prestations / travaux de tiers
440;Prestations / travaux de tiers
4400;Prestations / travaux de tiers
45;Charges d‘énergie
450;Charges d‘énergie pour l‘exploitation
4500;Charges d‘énergie pour l‘exploitation
49;Déductions sur les charges
490;Déductions sur les charges
4900;Déductions sur les charges
5;Charges de personnel
50;Salaires
500;Salaires
5000;Salaires
57;Charges sociales
5700;Charges sociales
58;Autres charges du personnel
580;Autres charges du personnel
5800;Autres charges du personnel
59;Charges de personnels temporaires
590;Charges de personnels temporaires
5900;Charges de personnels temporaires
6;Autres charges d‘exploitation, Amortissements et ajustement de valeur, Résultat financier
60;Charges de locaux
600;Charges de locaux
6000;Charges de locaux
61;Entretien, réparations et remplacement des inst.
610;Entretien, réparations et remplacement des inst. servant à l’exploitation
6100;Entretien, réparations et remplacement des inst. servant à l’exploitation
6105;Leasing immobilisations corporelles meubles
62;Charges de véhicules et de transport
620;Charges de véhicules et de transport
6200;Charges de véhicules et de transport
626;Leasing et location
6260;Leasing et location de véhicules
63;Assurances-choses, droits, taxes, autorisations
630;Assurances-choses, droits, taxes, autorisations
6300;Assurances-choses, droits, taxes, autorisations
64;Charges d’énergie et évacuation des déchets
640;Charges d’énergie et évacuation des déchets
6400;Charges d’énergie et évacuation des déchets
65;Charges d‘administration
650;Charges d‘administration
6500;Charges d‘administration
657;Charges et leasing d’informatique
6570;Charges et leasing d’informatique
66;Publicité
660;Publicité
6600;Publicité
67;Autres charges d‘exploitation
670;Autres charges d‘exploitation
6700;Autres charges d‘exploitation
68;Amortissement
680;Amortissement et ajustement de valeur des postes sur immobilisations corporelles
6800;Amortissement et ajustement de valeur des postes sur immobilisations corporelles
69;Charges financières
690;Charges
6900;Charges financières
695;Produits
6950;Produits financiers
7;Résultat des activités annexes d‘exploitation
70;Produits
700;Produits accessoires
7000;Produits accessoires
701;Charges accessoires
7010;Charges accessoires
75;Immeubles d‘exploitation
750;Produits des immeubles d‘exploitation
7500;Produits des immeubles d‘exploitation
751;Charges des immeubles d‘exploitation
7510;Charges des immeubles d‘exploitation
8;Résultats extraordinaires et hors exploitation
80;Charges
800;Charges hors exploitation
8000;Charges hors exploitation
81;Produits
810;Produits hors exploitation
8100;Produits hors exploitation
85;Autres charges
850;Charges extraordinaires, exceptionnelles ou hors période
8500;Charges extraordinaires, exceptionnelles ou hors période
851;Produits extraordinaires, exceptionnels ou hors période
8510;Produits extraordinaires, exceptionnels ou hors période
89;Impôts
890;Impôts directs
8900;Impôts directs
9;Clôture
90;Subventions, taxes de séjour et similaires. Contributions versées aux établissements chargés de l’élimination des déchets et de l’approvisionnement en eau
900;Subventions, taxes de séjour et similaires. Contributions versées aux établissements chargés de l’élimination des déchets et de l’approvisionnement en eau
91;Dons, les dividendes, les dédommagements
910;Dons, les dividendes, les dédommagements
92;Bénéfice / perte
920;Bénéfice / perte de l’exercice
9200;Bénéfice / perte de l’exercice