from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django import views
from django.conf import settings
from django.views.generic import ListView, CreateView, FormView, DetailView
from django.utils.translation import ugettext_lazy as _
from formtools.wizard.views import SessionWizardView
from vat.models import CompanyModel
from django.db import transaction as db_transaction

from hordak import models as hordak_models
from hordak import views as hordak_views
from moneyed import Money

from . import forms
from . import models
from . import utils

def main(request):
    context = {}
    company = CompanyModel.objects.all().first()
    if company != None:
        settings = models.accountentSettings.objects.filter(company=company).first()
        if settings != None:
            bank_account_list = utils.get_bank_accounts()
            if bank_account_list != None:
                return render(request, "accountent/main.html", context=context)
            else:
                redirect(reverse_lazy("accountent:bank_account_list"))
        
    return redirect(reverse_lazy('accountent:register_wizard'))

class AccountCreateView(hordak_views.AccountCreateView):
    template_name = "accountent/account_create.html"

class AccountDetailView(hordak_views.AccountTransactionsView):
    template_name = "accountent/account_detail.html"

class BankAccountDetailView(DetailView):
    model = models.BankAccount
    template_name = "accountent/bank_account_detail.html"

    def get_queryset(self):
        """
        Return the `QuerySet` that will be used to look up the object.
        Note that this method is called by the default implementation of
        `get_object` and may not be called if `get_object` is overridden.
        """
        if self.queryset is None:
            if self.model:
                return self.model._default_manager.all()
            else:
                raise ImproperlyConfigured(
                    "%(cls)s is missing a QuerySet. Define "
                    "%(cls)s.model, %(cls)s.queryset, or override "
                    "%(cls)s.get_queryset()." % {
                        'cls': self.__class__.__name__
                    }
                )
        bank_parent = hordak_models.Account.objects.filter(full_code=102).first()
        return self.queryset.filter(parent=bank_parent).all()

    def get_object(self, queryset=None):
        """
        Returns the object the view is displaying.
        By default this requires `self.queryset` and a `code` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()
        # Next, try looking up by code.
        code = self.kwargs.get('code')
        if code is not None:
            queryset = queryset.filter(account__code=code)
        
        # If none of those are defined, it's an error.
        if code is None:
            raise AttributeError("Generic detail view %s must be called with "
                                "either an object code."
                                % self.__class__.__name__)
        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                        {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Add legs for account
        context['legs'] = hordak_models.Leg.objects.filter(account=self.object.account)
        return context

class BankAccountCreateView(FormView):
    form_class = forms.CreateBankAccountForm
    template_name = "accountent/bank_account_create.html"
    success_url = reverse_lazy('accountent:bank_account_list')

    def form_valid(self, form):
        is_valid = super().form_valid(form)
        if is_valid:
            #create account
            account_name = form.cleaned_data.get('name')
            account_obj = utils.create_bank_account(account_name)

            #create bank
            bank_name = form.cleaned_data.get('bank_name')
            bank_obj, _ = models.Bank.objects.get_or_create(name=bank_name)

            #create bank account
            iban_num = form.cleaned_data.get('iban_number')
            swift_bic = form.cleaned_data.get('swift_bic')
            bank_account_obj = models.BankAccount.objects.create(account=account_obj, bank=bank_obj, iban=iban_num, swift_bic=swift_bic)

            return redirect(reverse_lazy("accountent:bank_account_detail", kwargs={'code':account_obj.code}))

        return is_valid

class BankAccountCreateImportView(hordak_views.statement_csv_import.CreateImportView):
    template_name = "accountent/statement_import/import_create.html"
    #success_url = reverse_lazy('accountent:bank_account_list')
    #form_class = forms.BankTransactionCsvImportForm
        
    def get_success_url(self):
        return reverse_lazy("accountent:bank_import_setup", args=[self.object.uuid])


class AccountListView(hordak_views.AccountListView):
    template_name = "accountent/account_list.html"
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active_id'] = "accounts-list"
        return context

class BankAccountListView(hordak_views.AccountListView):
    template_name = "accountent/bank_account_list.html"
    paginate_by = 10

    def get_queryset(self):
        parent = hordak_models.Account.objects.filter(full_code=102).first()
        queryset = hordak_models.Account.objects.filter(parent=parent, is_bank_account=True)
        return queryset #super().get_queryset()
    

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active_id'] = "bank-list"
        return context

   
class BankAccountSetupView(hordak_views.statement_csv_import.SetupImportView):
    template_name = "accountent/statement_import/import_setup.html"

    def get_success_url(self):
        return reverse_lazy("accountent:bank_import_dry_run", args=[self.object.uuid])

class BankDryRunImportView(hordak_views.statement_csv_import.DryRunImportView):
    template_name = "accountent/statement_import/import_dry_run.html"
    
    def get_success_url(self):
        return reverse_lazy("accountent:bank_import_execute", args=[self.object.uuid])

class BankExecuteImportView(hordak_views.statement_csv_import.ExecuteImportView):
    template_name = "accountent/statement_import/import_execute.html"

    def get_success_url(self):
        return reverse_lazy("accountent:main")

class TransactionReconcileListView(hordak_views.TransactionsReconcileView):
    template_name = "accountent/transactions/reconcile.html"

class SyncTransactionView(views.generic.RedirectView):

    pattern_name = 'accountent:transactions_list'

    def get_statement_line(self):
        sl = hordak_models.StatementLine.objects.order_by('date').all()
        unreconciled_sl = [x for x in sl if not x.is_reconciled]
        if len(unreconciled_sl) > 0:
            return unreconciled_sl[0]
        return None

    def get_redirect_url(self, *args, **kwargs):
        if self.get_statement_line():
            return reverse_lazy('accountent:transactions_reconcile_wizard')
        return super(SyncTransactionView, self).get_redirect_url(*args, **kwargs)

class TransactionListView(ListView):
    template_name = "accountent/transactions_list.html"
    model = hordak_models.Transaction
    list_display = [
        "pk",
        "timestamp",
        "debited_accounts",
        "credited_accounts",
        "total_amount",
    ]
    readonly_fields = ("timestamp",)
    search_fields = (
        "legs__account__name",
    )
    paginate_by = 10

    def debited_accounts(self, obj):
        return ", ".join([str(leg.account) for leg in obj.legs.debits()]) or None

    def credited_accounts(self, obj):
        return ", ".join([str(leg.account) for leg in obj.legs.credits()]) or None

    def total_amount(self, obj):
        return obj.legs.debits().aggregate(Sum("amount"))["amount__sum"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active_id'] = "transactions-journal"
        return context

class VATCodeModelListView(ListView):
    model = models.VATCodeModel
    template_name = "accountent/vat_code_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['active_id'] = "vat-code-list"
        return context