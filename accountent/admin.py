from django.contrib import admin

from . import models

class VATCodeAdmin(admin.ModelAdmin):
    pass

class TransactionAdmin(admin.ModelAdmin):
    pass

class accountentSettingsAdmin(admin.ModelAdmin):
    pass

admin.site.register(models.VATCodeModel, VATCodeAdmin)
admin.site.register(models.TransactionModel, TransactionAdmin)
admin.site.register(models.accountentSettings, accountentSettingsAdmin)