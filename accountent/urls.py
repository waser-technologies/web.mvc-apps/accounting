import os
from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views, wizards

app_name = "accountent"

urlpatterns = []

if os.environ.get('POSTGRES_HOST', False):
    from hordak import views as hordak_views

    urlpatterns += [
        url(r'^wizard/register/$', wizards.RegisterWizard.as_view(), name='register_wizard'),
        url(r'^wizard/transactions/create/$', wizards.TransactionCreateWizard.as_view(), name='transactions_create_wizard'),
        #url(r'^wizard/transactions/reconcile/$', wizards.TransactionReconcileWizard.as_view(), name='transactions_reconcile_wizard'),
        url(r'^transactions/$', views.TransactionListView.as_view(), name='transactions_list'),
        url(r'^transactions/create/$', hordak_views.TransactionCreateView.as_view(), name='transactions_create'),
        url(r'^transactions/currency/$', hordak_views.CurrencyTradeView.as_view(), name='currency_trade'),
        url(r'^transactions/reconcile/$', views.TransactionReconcileListView.as_view(), name='transactions_reconcile'),
        url(r'^transactions/sync/$', views.SyncTransactionView.as_view(), name='transactions_sync'),
        url(r'^account/$', views.AccountListView.as_view(), name='accounts_list'),
        url(r'^account/bank/$', views.BankAccountListView.as_view(), name='bank_account_list'),
        url(r'^account/bank/create/$', views.BankAccountCreateView.as_view(), name='bank_account_create'),
        url(r'^account/bank/import/create/$', views.BankAccountCreateImportView.as_view(), name='bank_import_create'),
        url(r'^account/bank/import/(?P<uuid>.*)/setup/$', views.BankAccountSetupView.as_view(), name='bank_import_setup'),
        url(r'^account/bank/import/(?P<uuid>.*)/dry-run/$', views.BankDryRunImportView.as_view(), name='bank_import_dry_run'),
        url(r'^account/bank/import/(?P<uuid>.*)/run/$', views.BankExecuteImportView.as_view(), name='bank_import_execute'),
        url(r'^account/bank/(?P<code>.*)/$', views.BankAccountDetailView.as_view(), name='bank_account_detail'),
        url(r'^account/create/$', views.AccountCreateView.as_view(), name='accounts_create'),
        url(r'^account/update/(?P<uuid>.+)/$', hordak_views.AccountUpdateView.as_view(), name='accounts_update'),
        url(r'^account/(?P<uuid>.+)/$', views.AccountDetailView.as_view(), name='accounts_transactions'),
        url(r'^settings/VAT/codes/$',views.VATCodeModelListView.as_view(), name='vat_code_list'),
        url(r'^$', login_required(views.main), name='main'),
    ]
