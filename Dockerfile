FROM python:3.6-stretch

COPY . web_mvc/
COPY requirements.txt web_mvc/requirements.txt
COPY requirements-prod.txt web_mvc/requirements-prod.txt
COPY requirements-git.txt web_mvc/requirements-git.txt

#COPY celeryd.init /etc/init.d/celeryd

#RUN chmod 755 /etc/init.d/celeryd
#RUN chown root:root /etc/init.d/celeryd

#COPY celeryd.conf /etc/default/celeryd

WORKDIR /web_mvc

RUN pip install --upgrade pip

RUN pip install -r requirements-prod.txt

RUN pip install -r requirements-git.txt

EXPOSE 8000
EXPOSE 5432

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

COPY start.sh .
RUN chmod 755 ./start.sh

CMD ["./start.sh"]