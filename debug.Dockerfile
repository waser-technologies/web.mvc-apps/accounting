FROM python:3.6-stretch

COPY . web_mvc/
COPY requirements.txt web_mvc/requirements.txt
COPY requirements-prod.txt web_mvc/requirements-prod.txt
COPY requirements-git.txt web_mvc/requirements-git.txt

WORKDIR /web_mvc

EXPOSE 8000

RUN pip install -r requirements-prod.txt

RUN pip install -r requirements-git.txt

RUN pip install ptvsd

RUN python manage.py migrate --no-input
RUN python manage.py collectstatic --no-input

RUN echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@waser.tech', 'Test')" | python manage.py shell

CMD ["python", "-m", "ptvsd", "--host", "0.0.0.0", "--port", "5678", "--wait", \
    "manage.py", "runserver", "--noreload", "--nothreading", "0.0.0.0:8000"]
